import requests
from bs4 import BeautifulSoup


def tenki():
    url = "https://weather.yahoo.co.jp/weather/jp/13/4410.html"
    r = requests.get(url)

    if r.status_code == requests.codes.ok:
        soup = BeautifulSoup(r.content, "html.parser")
        print(soup.select_one("div.forecastCity p.pict"))


def chien():
    url = "https://transit.yahoo.co.jp/traininfo/detail/21/0/"
    r = requests.get(url)
    if r.status_code == requests.codes.ok:
        soup = BeautifulSoup(r.content, "html.parser")
        print(soup.select_one("div#mdServiceStatus dt"))


def main():
    pass


if __name__ == "__main__":
    tenki()
    chien()
